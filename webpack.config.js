const path = require('path');
const glob = require('glob');
const webpack = require('webpack');
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const PurgecssPlugin = require('purgecss-webpack-plugin');

const PATHS = {
  src: path.join(__dirname, 'src')
};

module.exports = {
  context: __dirname,
  entry: [
      'babel-polyfill', './src/index.js'
  ],

  output: {
      path: path.resolve('./files/staticfiles/build/'),
      filename: 'app.js'
  },

  module: {
      rules: [
      {
        test: /\.(js|jsx)$/,
        exclude: /node_modules/,
        use: ['babel-loader'],
      },
      {
        test: /\.sass$/,
        use:[
            'postcss-loader',
            MiniCssExtractPlugin.loader,
            {
              loader: 'css-loader',
              options: {
                url: false,
                minimize: true || {/* CSSNano Options */}
              }
            },
            'sass-loader',
        ]
      }
    ]
  },
  plugins: [
    new MiniCssExtractPlugin({
      filename: "style.css"
    }),
  ],
  resolve: {
    extensions: ['*', '.js', '.jsx']
  },
};