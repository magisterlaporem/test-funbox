import React from "react";
const classNames = require('classnames');

function createText(option) {
  return {__html: option};
}


class CatFeed extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            'hover': false
        };
        this.selectFeed = this.selectFeed.bind(this);
        this.unselectFeed = this.unselectFeed.bind(this);
        this.mouseMove = this.mouseMove.bind(this);
        this.mouseLeave = this.mouseLeave.bind(this)
    }

    selectFeed(){
        this.props.selectFeed(this.props.index)
    }

    unselectFeed(){
        console.log("unselectFeed")
    }

    mouseMove(){
        if (this.props.select && !this.state.hover) this.setState({hover: true})
    }

    mouseLeave(){
        this.setState({hover: false})
    }

    render() {
        let options = this.props.feed.option.map((option, index) =>
            <text key={index} className='options' x="50" y={155+index*19}dangerouslySetInnerHTML={createText(option)}></text>
        );
        let Classes = classNames(
            'catFeed',
            {
                'select': this.props.select,
                'disabled': this.props.disabled
            }
        );
        let preText = (this.state.hover) ?
            <text className='preTitle hover' x="50" y="38" onClick={this.selectFeed}>Котэ не одобряет?</text> :
            <text className='preTitle' x="50" y="38">Сказочное заморское яство</text>

        let action;
        if (!this.props.disabled) {
            action = (this.props.select) ?
            <div className='action'>{this.props.feed.select_text}</div> :
            <div className='action'>Чего сидишь? Порадуй котэ, <a onClick={this.selectFeed}>купи.</a></div>
        }else{
            action = <div className='action'>Печалька, {this.props.feed.with} закончился.</div>
        }

        let disabled = (this.props.disabled) ? <svg className='disableCart' viewBox="0 0 326 486">
                    <path className='cartPath' fillRule="evenodd"  stroke="rgb(179, 179, 179)" strokeWidth="4px" strokeLinecap="butt" strokeLinejoin="miter" fill="rgb(242, 242, 242)"
                     d="M310.000,482.000 L14.000,482.000 C7.373,482.000 2.000,476.627 2.000,470.000 L2.000,45.000 L45.000,2.000 L310.000,2.000 C316.627,2.000 322.000,7.373 322.000,14.000 L322.000,470.000 C322.000,476.627 316.627,482.000 310.000,482.000 Z"/>
                    </svg>: null;

        return (
            <div className={Classes} onMouseLeave={this.mouseLeave} onMouseMove={this.mouseMove}>
                <div className='informationData'>


                    <svg className='cart' viewBox="0 0 326 486">
                        <path className='cartPath' fillRule="evenodd"  strokeWidth="4" strokeLinecap="butt" strokeLinejoin="miter"
                         d="M310.000,482.000 L14.000,482.000 C7.373,482.000 2.000,476.627 2.000,470.000 L2.000,45.000 L45.000,2.000 L310.000,2.000 C316.627,2.000 322.000,7.372 322.000,14.000 L322.000,470.000 C322.000,476.627 316.627,482.000 310.000,482.000 Z"/>
                    </svg>

                    <svg className='cat' viewBox="0 0 326 486">
                        <defs>
                            <clipPath id="clipping">
                                 <path id='star' fillRule="evenodd"  stroke="rgb(22, 152, 217)" strokeWidth="4" strokeLinecap="butt" strokeLinejoin="miter"
                                 d="M310.000,480.000 L14.000,480.000 C7.373,480.000 4.000,476.627 4.000,470.000 L4.000,45.000 L45.000,4.000 L310.000,4.000 C316.627,4.000 322.000,7.372 322.000,14.000 L322.000,470.000 C322.000,476.627 316.627,480.000 310.000,480.000 Z"/>
                            </clipPath>
                        </defs>
                        <image x='2' y='2' height="486" width="326" className='image' clipPath='url(#clipping)' xlinkHref="img/cat.png"/>
                        <g>
                            <circle className='circle' cx="268" cy="428" r="40"></circle>
                            {preText}
                            <text className='title' x="50" y="90">Нямушка</text>
                            <text className='with' x="50" y="125">{this.props.feed.with}</text>

                            {options}


                            <text className='weight' x="267" y="418" dy=".3em">{this.props.feed.weight}</text>
                            <text className='kg' x="267" y="448" dy=".3em">кг</text>
                        </g>
                    </svg>

                    {disabled}
                </div>
                {action}
            </div>
        );
    }
}

export default CatFeed