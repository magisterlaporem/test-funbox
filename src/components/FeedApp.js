import React from "react";
import CatFeed from "./CatFeed";


class FeedApp extends React.Component {
    constructor(props) {
        super(props);
        this.state={
            feeds: [
                {
                    weight: '0,5',
                    with: 'c фуа-гра',
                    select_text: 'Печень утки разварная с артишоками.   ',
                    option:[
                        '<tspan>10</tspan> порций',
                        'мышь в подарок',
                    ]
                },
                {
                    weight: '2',
                    with: 'c рыбой',
                    select_text: 'Головы щучьи с чесноком да свежайшая сёмгушка.',
                    option:[
                        '<tspan>40</tspan> порций',
                        'мышь в подарок',
                        '<tspan>2</tspan> мыши в подарок',
                    ],
                },
                {
                    weight: '5  ',
                    with: 'c курой',
                    select_text: 'Филе из цыплят с трюфелями в бульоне.',
                    option:[
                        '<tspan>100</tspan> порций',
                        '<tspan>5</tspan> мышей в подарок',
                        'заказчик доволен',
                    ],
                    disabled: true
                },
            ],
            selected: [1],
        };
        this.selectFeed = this.selectFeed.bind(this)
    }

    selectFeed(index){
        console.log(index);
        let array = [...this.state.selected];
        let indexSearch = array.indexOf(index);
        if (indexSearch == -1){
            array.push(index);
            this.setState({selected: array})
        }else{
            array = array.splice(index, 1);
            this.setState({selected: array});

        }
    }

    render() {
        let feeds = this.state.feeds.map((feed, index) =>
            <CatFeed disabled={feed.disabled} select={(this.state.selected.indexOf(index) != -1 )} selectFeed={this.selectFeed} index={index} key={index} feed={feed} />
        );
        return (
            <div className='content'>
                <div className='feedsTitle'>
                    Ты сегодня покормил кота?
                </div>
                <div className='feedsContent'>
                    {feeds}
                </div>
            </div>
        );
    }
}

export default FeedApp