"use strict";
import React from 'react';
import ReactDOM from 'react-dom';
import './style.sass';
import FeedApp from "./components/FeedApp";


ReactDOM.render(
            <div id='feeds'>
                <div className='shadow'></div>
                {<FeedApp/>}
            </div>,
    document.getElementById('root')
);
